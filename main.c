/* DSA Project 2017: Word Count.
 * In man wc, editors like gedit, vi, vim, nano automatically put a newline character at the end of the file. So, when you check the no.of 
 * characters present, there's one extra character, due to the added newline. So, to avoid this, in vi & vim, there's commands :set noeol & 
 * :set binary. These then set the mode in which that extra newline doesn't appear. In nano, use --nonewline flag.
 * wc takes care of the following:
 * 1. Whenever apostrophe is present, it treats it as part of the given word.
 * 2. Counts no.of words, characters, lines, bytes, in the file. Specific ones with appropriate flags.
 * 3. Works on multiple files as well. At the end, it gives the total word count in all the mentioned files. 
 * 4. Handles errors on no file given, permission denied, is a directory.
 * 5. It also works on binary files and zipped files.
 * 6. Also works if I tell it to count all the words present in all the files in the directory supplied by user.
 * 7. Contains ignore options like ignore vowels, or ignore words less then given length (-ign 3)
 * 8. Also counts paragraphs, whenever an empty line is encountered.
 */
